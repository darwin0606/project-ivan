<?php
/**
 * Created by PhpStorm.
 * User: Darwin
 * Date: 23/11/2018
 * Time: 8:40
 */
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('main/home');
    }

    public function sign_up()
    {
        return redirect(route('userProfile'));
    }

    public function login()
    {
        return redirect(route('userProfile'));
    }
}
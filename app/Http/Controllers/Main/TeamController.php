<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27/11/2018
 * Time: 15:48
 */
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index()
    {
        return view('main/teams/team_list');
    }

    public function game($game_id)
    {
        return view('main/teams/team_game');
    }
}
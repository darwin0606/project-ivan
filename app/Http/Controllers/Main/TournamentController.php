<?php
/**
 * Created by PhpStorm.
 * User: Darwin
 * Date: 23/11/2018
 * Time: 8:40
 */
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

class TournamentController extends Controller
{
    public function index()
    {
        return view('main/tournament/tournament_list');
    }

    public function game($game_id)
    {
        return view('main/tournament/tournament_game');
    }

    public function detail($game_id, $tournament_id)
    {
        return view('main/tournament/tournament_detail');
    }

    public function register($game_id, $tournament_id)
    {
        return view('main/tournament/tournament_register');
    }

    public function register_post($game_id, $tournament_id)
    {
        $tournament_register_id = 1;
        return redirect(route('tournamentPay', $tournament_register_id));
    }

    public function pay($tournament_register_id)
    {
        return view('main/tournament/tournament_pay');
    }

    public function pay_post($tournament_register_id)
    {
        session(['pay' => true]);
        return redirect(route('tournamentDetail', [1, 1]));
    }

    public function team($game_id, $tournament_id)
    {
        return view('main/tournament/tournament_team');
    }
}
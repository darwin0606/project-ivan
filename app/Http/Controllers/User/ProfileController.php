<?php
/**
 * Created by PhpStorm.
 * User: Darwin
 * Date: 23/11/2018
 * Time: 8:40
 */
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        return view('user/profile/profile');
    }

    public function edit()
    {
        return view('user/profile/profile_edit');
    }

    public function edit_post()
    {
        return redirect(route('userProfile'));
    }

    public function team()
    {
        return view('user/profile/profile_team');
    }
}
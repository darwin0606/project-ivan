@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>Turnamen Dota 2 - Fox Howl Rookies</h6>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="tour">
                        <div class="j242 j239" style="background-image:url({{asset('images/rookie.jpg')}})" title=""></div>
                        <div class="j234">

                            <div class="tour-desc">
                                <h4>REGISTRATION - Turnamen Dota 2 - Fox Howl Rookies</h4>


                                <form method="post" action="{{route('tournamentRegisterPost', [1, 1])}}" class="text-left">
                                    <div class="form-group">
                                        <label for="email">Player 1</label>
                                        <input type="email" class="form-control" id="Username">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Player 2</label>
                                        <input type="email" class="form-control" id="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Player 3</label>
                                        <input type="password" class="form-control" id="pwd">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Player 4</label>
                                        <input type="password" class="form-control" id="pwd">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Player 5</label>
                                        <input type="password" class="form-control" id="pwd">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Player Cadangan</label>
                                        <input type="password" class="form-control" id="pwd">
                                    </div>
                                    <div class="text-center">
                                        <input type="submit" class="btn btn-warning mainbtn" value="Register">
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                    <div class="col-2"></div>
                </div>
            </div>

            {{--<div class="page-content">
                <div class="ttl">
                Popular Games
                </div>
            </div>--}}
        </div>



    </div>

</div>
</div>
</main>
@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>Tournaments by Game</h6>
            <div class="row">
                <div class="col-8 col-xl-6 .col-lg-8">
                    <div class="row">
                        <div class="col-6">
                            <a href="{{route('tournament', 1)}}">
                                <div class="tour-category">
                                    <div class="img-ctn">
                                        <img src="{{asset('images/dota.png')}}" alt="" width="100%">
                                    </div>
                                    <div class="dvder">
                                        <p>
                                            Dota 2
                                        </p>
                                        <span>
                                        15 Tournaments
                                    </span>
                                    </div>
                                </div>
                            </a>

                            <div class="tour-category">
                                <div class="img-ctn">
                                    <img src="{{asset('images/csgo.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        CS GO
                                    </p>
                                    <span>
                                        8 Tournaments
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="tour-category">
                                <div class="img-ctn">
                                    <img src="{{asset('images/mole.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        Mobile Legends
                                    </p>
                                    <span>
                                        5 Tournaments
                                    </span>
                                </div>
                            </div>

                            <div class="tour-category">
                                <div class="img-ctn">
                                    <img src="{{asset('images/aov.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        Arena of Valor
                                    </p>
                                    <span>
                                        20 Tournaments
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4 col-xl-6 .col-lg-4"></div>
            </div>
        </div>

        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>All Tournaments</h6>
            <div class="row">
                <div class="col-4 col-xl-3 .col-lg-4">
                    <a href="{{route('tournamentDetail', [1, 1])}}">
                        <div class="tour">
                            <div class="j242 j238" style="background-image:url({{asset('images/dotatour.jpg')}})" title=""></div>
                            <div class="j234">
                                <div class="tour-pricepool">
                                    <div><span class="icon-winner"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span></span> 5 Juta</div>
                                </div>
                                <div class="tour-category-in">
                                    <div class="img-ctn">
                                        <img src="{{asset('images/dota.png')}}" alt="" width="100%">
                                    </div>
                                    <div class="dvder">
                                        <p>
                                            Dota
                                        </p>
                                        <span>
                                        20 Slots
                                    </span>
                                    </div>
                                </div>
                                <div class="slot-ctn">
                                    <div class="ttl-slot">
                                        50 Slots
                                    </div>
                                    <div class="left-slot">
                                        6 Slots Left
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-4 col-xl-3 .col-lg-4">
                    <div class="tour">
                        <div class="j242 j238" style="background-image:url({{asset('images/csgot.png')}})" title=""></div>
                        <div class="j234">
                            <div class="tour-pricepool">
                                <div><span class="icon-winner"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span></span> 2 Juta</div>
                            </div>
                            <div class="tour-category-in">
                                <div class="img-ctn">
                                    <img src="{{asset('images/csgo.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        Counter Strike GO
                                    </p>
                                    <span>
                                        20 Slots
                                    </span>
                                </div>
                            </div>
                            <div class="slot-ctn">
                                <div class="ttl-slot">
                                    50 Slots
                                </div>
                                <div class="left-slot">
                                    6 Slots Left
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-4 col-xl-3 .col-lg-4">
                    <div class="tour">
                        <div class="j242 j238" style="background-image:url({{asset('images/aov.jpg')}})" title=""></div>
                        <div class="j234">
                            <div class="tour-pricepool">
                                <div><span class="icon-winner"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span></span> 3 Juta</div>
                            </div>
                            <div class="tour-category-in">
                                <div class="img-ctn">
                                    <img src="{{asset('images/aov.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        Arena of Valor
                                    </p>
                                    <span>
                                        20 Slots
                                    </span>
                                </div>
                            </div>
                            <div class="slot-ctn">
                                <div class="ttl-slot">
                                    50 Slots
                                </div>
                                <div class="left-slot">
                                    6 Slots Left
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        {{--<div class="page-content">
            <div class="ttl">
            Popular Games
            </div>
        </div>--}}
        @include('main.footer')

    </div>


</div>
</div>
</div>
</main>


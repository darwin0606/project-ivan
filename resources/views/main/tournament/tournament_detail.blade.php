@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>Turnamen Dota 2 - Fox Howl Rookies</h6>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="tour">
                        <div class="j242 j239" style="background-image:url({{asset('images/rookie.jpg')}})" title=""></div>
                        <div class="j234">
                            <div class="tour-desc">
                                    <h4>Turnamen Dota 2 - Fox Howl Rookies</h4>


                                <p><b>- Detail Turnamen :</b></p>
                                <ul>
                                    <li>Match Day : 3 - 4 November 2018</li>
                                    <li>Pendaftaran akan ditutup tanggal 1 November 2018</li>
                                    <li>TM tanggal 2 November 2018, Jam 19.00 WIB via Discord</li>
                                    <li>Biaya pendaftaran IDR 50.000,- / Slot</li>
                                    <li>Link pendaftaran : <a href="https://bit.ly/2Qpgscv">https://bit.ly/2Qpgscv</a></li>
                                    <li>Venue : Online</li>
                                    <li>Open 32 Slot</li>
                                    <li>Multi Slot On</li>
                                    <li>Double Winner Allowed</li>
                                    <li>Max Medal Legend 3</li>
                                    <li>Single Elimination Mode</li>
                                </ul>

                                <p><b>- Hadiah :</b></p>
                                <ul>
                                    <li>Juara 1 : Rp. 500.000,- + Team Profile di Website Info Tourney</li>
                                    <li>Juara 2 : Rp. 300.000,- + Team Profile di Website Info Tourney</li>
                                    <li>Juara 3 : Rp. 200.000,- + Team Profile di Website Info Tourney</li>
                                    <li><small>*Hadiah bisa berubah sesuai dengan jumlah peserta</small></li>
                                </ul>

                                <p><b>- Contact Person :</b></p>
                                <ul>
                                    <li>WA : 082190782042</li>
                                    <li>WA : 087780407740</li>
                                    <li><a href="https://steamcommunity.com/id/yustinus/">Steam Profile : https://steamcommunity.com/id/yustinus/</a></li>
                                    <li><a href="https://steamcommunity.com/profiles/76561198260032296/">Steam Profile : https://steamcommunity.com/profiles/76561198260032296/</a></li>
                                </ul>

                                <p><b>- Laporan Penipuan Turnamen :</b></p>
                                <ul>
                                    <li>Laporan penipuan turnamen : <a href="penipuan@infotourney.com">penipuan@infotourney.com</a></li>
                                </ul>

                            </div>

                            @if (session('pay') == false)
                                <div class="countdown" style="background: transparent">
                                    <div class="countdown-title">Batas waktu pendaftaran</div>
                                    <div class="countdown-time" id="countdown-time" style="color: #FF5722;"></div>
                                </div>                            <br>

                                <a class="btn btn-warning mainbtn" href="{{route('tournamentRegister', [1, 1])}}">Daftar <small>( tersisa <b>3</b> slot )</small></a>
                            @else
                            <div class="alert alert-primary" role="alert">
                                <h4 style="font-size: 21px;font-weight: 600;" class="alert-heading">Team Anda sudah terdaftar</h4>
                                <p style="margin-bottom:0;">Klik <b data-toggle="collapse" href="#showteam" role="button" aria-expanded="false"><i><u>disini</u></i></b> untuk melihat tim yang telah terdaftar</p>

                                <div class="collapse" id="showteam">
                                       <div class="row">
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/virtuspro.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Virtus Pro <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/liquid.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Liquid <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/eg.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team EG <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/dc.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Digital Chaos <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/infamous.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Infamous <br>
                                                           <span class="badge badge-secondary">Waiting for payment</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/invictus.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Infictus Gaming <br>
                                                           <span class="badge badge-secondary">Waiting for payment</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/newbee.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Newbee <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/og.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team OG <br>
                                                           <span class="badge badge-secondary">Waiting for payment</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/secret.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Secret <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/spirit.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Spirit <br>
                                                           <span class="badge badge-secondary">Waiting for payment</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-6">
                                               <div class="registered-team">
                                                   <div class="img-ctn">
                                                       <img src="{{env('APP_URL')}}/images/tnctiger.png" alt="" width="100%">
                                                   </div>
                                                   <div class="dvder">
                                                       <p>
                                                           Team Tiger <br>
                                                           <span class="badge badge-success">Confirmed</span>
                                                       </p>

                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-2"></div>
            </div>
        </div>
    </div>
</div>
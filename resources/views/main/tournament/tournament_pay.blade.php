@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>Turnamen Dota 2 - Fox Howl Rookies</h6>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="tour">
                        <div class="j242 j239" style="background-image:url({{asset('images/rookie.jpg')}})" title=""></div>
                        <div class="j234">

                            <div class="tour-desc">
                                <h4>PAYMENT - Turnamen Dota 2 - Fox Howl Rookies</h4>


                                <p>Informasi Pembayaran</p>
                                <div class="payAccounts">
                                    <div class="bank-ctn" style="padding-top: 0px;">
                                        <div class="payAccount bank">
                                            <img src="https://api.dealjava.com/content/bank_icon/bca.png" alt="bca">
                                            <span>7860098908</span>
                                        </div>
                                        <span style="color: rgb(228, 227, 227);font-size: 12px;">a.n. PT DIGITAL MEDIA GROUP</span>
                                    </div>
                                    <div>
                                        <div class="countdown">
                                            <div class="countdown-title">Sisa Waktu Pembayaran</div>
                                            <div class="countdown-time" id="countdown-time"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <p>Upload Bukti Pembayaran</p>
                                    <form method="post" action="{{route('tournamentPayPost', 1)}}" class="dropzone" style="margin-left:0;">
                                        <div class="fallback">
                                            <input name="file" type="file" multiple />
                                        </div>
                                        <br>
                                        <input type="submit" class="btn btn-warning mainbtn" style="display: block;width:100%;" value="Submit">
                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="col-2"></div>
                </div>
            </div>

            {{--<div class="page-content">
                <div class="ttl">
                Popular Games
                </div>
            </div>--}}
        </div>



    </div>

</div>
</div>
</main>
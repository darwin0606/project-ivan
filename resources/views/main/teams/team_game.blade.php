@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>Teams by Game</h6>
            <div class="row">
                <div class="col-8 col-xl-6 .col-lg-8">
                    <div class="row">
                        <div class="col-6">
                            <a href="{{route('team', 1)}}">
                                <div class="tour-category">
                                    <div class="img-ctn">
                                        <img src="{{asset('images/dota.png')}}" alt="" width="100%">
                                    </div>
                                    <div class="dvder">
                                        <p>
                                            Dota 2
                                        </p>
                                        <span>
                                        15 Teams
                                    </span>
                                    </div>
                                </div>
                            </a>

                            <div class="tour-category">
                                <div class="img-ctn">
                                    <img src="{{asset('images/csgo.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        CS GO
                                    </p>
                                    <span>
                                        8 Teams
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="tour-category">
                                <div class="img-ctn">
                                    <img src="{{asset('images/mole.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        Mobile Legends
                                    </p>
                                    <span>
                                        5 Teams
                                    </span>
                                </div>
                            </div>

                            <div class="tour-category">
                                <div class="img-ctn">
                                    <img src="{{asset('images/aov.png')}}" alt="" width="100%">
                                </div>
                                <div class="dvder">
                                    <p>
                                        Arena of Valor
                                    </p>
                                    <span>
                                        20 Teams
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4 col-xl-6 .col-lg-4"></div>
            </div>
        </div>

        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>Dota Teams</h6>
            <div class="row">
                <div class="col-6 col-xl-6 .col-lg-6">
                    <div class="myteam" style="box-shadow: 1px 2px 10px 0px rgba(0, 0, 0, 0.3);">
                        <div class="team-category">
                            <div class="img-ctn">
                                <img src="{{asset('images/eg.png')}}" alt="" width="100%">
                            </div>
                            <div class="dvder">
                                <p>
                                    Evil Genius <br>
                                    <span style="font-weight: 300">DOTA 2</span>
                                </p>

                            </div>
                        </div>
                        <div class="teamplayer">
                            <div class="row text-center">
                                <div class="col-4">
                                    <div class="profile" style="height:auto;background: transparent;float:none;width:100%">
                                        <div class="image">
                                            <div class="circle-1"></div>
                                            <div class="circle-2"></div>
                                            <img src="{{asset('images/gplayer.jpg')}}" width="70" height="70" alt="Jessica Potter">
                                        </div>

                                        <div class="name">Jessica Potter</div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="profile" style="height:auto;background: transparent;float:none;width:100%">
                                        <div class="image">
                                            <div class="circle-1"></div>
                                            <div class="circle-2"></div>
                                            <img src="{{asset('images/gplayer.jpg')}}" width="70" height="70" alt="Jessica Potter">
                                        </div>

                                        <div class="name">Jessica Potter</div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="profile" style="height:auto;background: transparent;float:none;width:100%">
                                        <div class="image">
                                            <div class="circle-1"></div>
                                            <div class="circle-2"></div>
                                            <img src="{{asset('images/gplayer.jpg')}}" width="70" height="70" alt="Jessica Potter">
                                        </div>

                                        <div class="name">Jessica Potter</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="profile" style="height:auto;background: transparent;float:none;width:100%">
                                        <div class="image">
                                            <div class="circle-1"></div>
                                            <div class="circle-2"></div>
                                            <img src="{{asset('images/gplayer.jpg')}}" width="70" height="70" alt="Jessica Potter">
                                        </div>

                                        <div class="name">Jessica Potter</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="profile" style="height:auto;background: transparent;float:none;width:100%">
                                        <div class="image">
                                            <div class="circle-1"></div>
                                            <div class="circle-2"></div>
                                            <img src="{{asset('images/gplayer.jpg')}}" width="70" height="70" alt="Jessica Potter">
                                        </div>

                                        <div class="name">Jessica Potter</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="teamprestasi">
                            <p style="margin-bottom:5px;"><b>Prestasi Team</b></p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ... The first word, “Lorem,” isn't even a word; instead it's a piece of the word “dolorem,” meaning pain, suffering, or sorrow.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('main.footer')
    </div>
</div>
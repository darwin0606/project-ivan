<nav id="toolbar">

    <div class="row no-gutters align-items-center flex-nowrap">

        <div class="col">

            <div class="row no-gutters align-items-center flex-nowrap">

                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none fuse-ripple-ready" data-fuse-bar-toggle="aside">
                    <i class="icon icon-menu"></i>
                </button>

                <div class="toolbar-separator d-block d-lg-none"></div>

                <div class="shortcuts-wrapper row no-gutters align-items-center px-0 px-sm-2">

                    <div class="shortcuts row no-gutters align-items-center d-none d-md-flex">

                        <a href="apps-chat.html" class="shortcut-button btn btn-icon mx-1 fuse-ripple-ready">
                            <i class="icon icon-hangouts"></i>
                        </a>

                        <a href="apps-contacts.html" class="shortcut-button btn btn-icon mx-1 fuse-ripple-ready">
                            <i class="icon icon-account-box"></i>
                        </a>

                        <a href="apps-mail.html" class="shortcut-button btn btn-icon mx-1 fuse-ripple-ready">
                            <i class="icon icon-email"></i>
                        </a>

                    </div>
                </div>

                <div class="toolbar-separator"></div>

            </div>
        </div>

        <div class="col-auto">

            <div class="row no-gutters align-items-center justify-content-end">
                <div class="loginbtn-ctn">
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myLogin">Login</button>
                    <button type="button" class="btn btn-sm  btn-secondary" data-toggle="modal" data-target="#mySignup">Sign Up</button>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="modal fade" id="myLogin">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content text-center">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Logo Here</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <p>Sign in to get started</p>
                <form method="post" action="{{route('login')}}" class="text-left">
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd">
                    </div>
                    <div class="form-group form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> Remember me
                        </label>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Sign In" style="display: block;width:100%;">
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="mySignup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content text-center">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Logo Here</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <p>Sign up to get started</p>
                <form method="post" action="{{route('signup')}}" class="text-left">
                    <div class="form-group">
                        <label for="email">Username</label>
                        <input type="email" class="form-control" id="Username">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Confirm Password:</label>
                        <input type="password" class="form-control" id="pwd">
                    </div>

                    <input type="submit" class="btn btn-warning" style="display: block;width:100%;" value="Sign Up">
                </form>
            </div>

        </div>
    </div>
</div>
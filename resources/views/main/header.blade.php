<?php
/**
 * Created by PhpStorm.
 * User: riyan
 * Date: 3/16/2017
 * Time: 10:56 AM
 */
?>
<html lang="id">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="language" content="Indonesian">
    <meta http-equiv="Content-Language" content="id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="geo.country" content="ID" />
    <meta name="author" content=""/>
    <meta name="copyright" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="icon" type="image/x-icon" href="">
    <title>  <?=isset($title)?$title:"UNTITLED"?> | </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('icomoon/style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="{{asset('scrollbar/dist/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('countdown/jquery-countdown.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/ivan.css')}}?v=2">

</head>
<script>
    $(document).ready(function(){
        const container = document.querySelector('#customscroll');
        const ps = new PerfectScrollbar(container);

        $("#countdown-time")
            .countdown("2019/01/01", function(event) {
                $(this).text(
                    event.strftime('%D days %H:%M:%S')
                );
            });

        $(".dropzone").dropzone()
    });

</script>
<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto:400,700');
    body{
        font-family: 'Roboto', sans-serif!important;
        background-color: #f1f2f3;
        font-size:15px;
    }

</style>
<body>
<main>
    <div id="wrapper">

<aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
    <div class="aside-content bg-primary-700 text-auto">

        <div class="aside-toolbar">
            <a href="{{route('home')}}" class="invilink">
            <div class="logo">
                <span class="logo-icon">T</span>
                <span class="logo-text">Tournament</span>
            </div>
            </a>

            <button id="toggle-fold-aside-button" type="button" class="btn btn-icon d-none d-lg-block fuse-ripple-ready" data-fuse-aside-toggle-fold="">
                <i class="icon icon-backburger"></i>
            </button>

        </div>

        <ul class="nav flex-column custom-scrollbar ps ps--active-y" id="sidenav" data-children=".nav-item">

            <li class="subheader">
                <span>MENU</span>
            </li>

            <li class="nav-item" role="tab">

                <a class="nav-link"  href="{{route('home')}}">

                    <span class="icon-home-page"></span>

                    <span>Home</span>
                </a>
            </li>
            <li class="nav-item" role="tab">
                <a class="nav-link" href="{{route('tournament-list')}}">

                    <span class="icon-trophy-shape"></span>

                    <span>All Tournaments</span>
                </a>
            </li>
            <li class="nav-item" role="tab">

                <a class="nav-link" href="{{route('team-list')}}">
                    <span class="icon-group"></span>
                    <span>All Teams</span>
                </a>
            </li>


        </ul>
    </div>

</aside>



<div class="content-wrapper"  id="customscroll">
    @include('main.navbar')


@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>My Profile</h6>
            <div class="row">
                <div class="col-4">
                    <div class="edit-profil">
                        <form method="post" action="{{route('userProfileEditPost')}}" class="text-left">
                            <div class="form-group">
                                <label for="">Foto Profil</label>
                                <input name="file" type="file" multiple />
                            </div>
                            <div class="form-group">
                                <label for="email">Username</label>
                                <input class="form-control" id="Username">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password</label>
                                <input type="password" class="form-control" id="pwd">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Confirm Password</label>
                                <input type="password" class="form-control" id="pwd">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Tentang Saya</label>
                                <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                            </div>

                            <input type="submit" class="btn btn-warning" style="display: block;width:100%;" value="Simpan">
                        </form>
                    </div>
                </div>
                <div class="col-8">
                </div>
            </div>
        </div>
    </div>
</div>
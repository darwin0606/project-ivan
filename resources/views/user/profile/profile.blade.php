@include('main.header')
<div class="content">
    <div class="page-content-wrapper">
        <div class="page-header text-auto d-flex flex-column justify-content-between px-6 pt-4">
            <h6>My Profile</h6>
            <div class="row">
                <div class="col-4">
                    <div class="frame">
                        <div class="center">

                            <div class="profile">
                                <div class="image">
                                    <div class="circle-1"></div>
                                    <div class="circle-2"></div>
                                    <img src="{{asset('images/gplayer.jpg')}}" width="70" height="70" alt="Jessica Potter">
                                </div>

                                <div class="name">Jessica Potter</div>
                                <div class="job">
                                    <p>Hi, I'm Riccardo Cavallo and I'm a Dota and Mobile legend player.Hi, I'm Riccardo Cavallo and I'm a Dota and Mobile legend player. Hi, I'm Riccardo Cavallo and I'm a Dota and Mobile legend player.</p>
                                </div>


                                <div class="actions">
                                    <a href="{{route('userProfileEdit')}}"  class="btn">Edit Profile</a>
                                </div>
                            </div>

                            <div class="stats">
                                <div class="box">
                                    <span class="value">3</span>
                                    <span class="parameter">Wins</span>
                                </div>
                                <div class="box">
                                    <span class="value" style="margin-bottom:3px;"><span class="icon-favorite-heart-button"></span></span>
                                    <span class="parameter">Achievement</span>
                                </div>
                                <a href="{{route('userProfileTeam')}}">
                                <div class="box">
                                    <span class="value"><span class="icon-group"></span></span>
                                    <span class="parameter">Teams</span>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">

                </div>
            </div>

        </div>



    </div>

</div>
</div>
</main>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('/')->namespace('Main')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('/signup', 'HomeController@sign_up')->name('signup');
    Route::post('/login', 'HomeController@login')->name('login');

    Route::prefix('/tournament')->group(function () {
        Route::get('/', 'TournamentController@index')->name('tournament-list');
        Route::get('/pay/{tournament_register_id}', 'TournamentController@pay')->name('tournamentPay');
        Route::post('/pay/{tournament_register_id}', 'TournamentController@pay_post')->name('tournamentPayPost');
        Route::get('/{game_id}', 'TournamentController@game')->name('tournament');
        Route::get('/{game_id}/{tournament_id}', 'TournamentController@detail')->name('tournamentDetail');
        Route::get('/register/{game_id}/{tournament_id}', 'TournamentController@register')->name('tournamentRegister');
        Route::post('/register/{game_id}/{tournament_id}', 'TournamentController@register_post')->name('tournamentRegisterPost');
    });

    Route::prefix('/team')->group(function () {
        Route::get('/', 'TeamController@index')->name('team-list');
        Route::get('/{game_id}', 'TeamController@game')->name('team');
    });
});

Route::prefix('/user')->namespace('User')->group(function () {
    Route::prefix('/profile')->group(function () {
        Route::get('/', 'ProfileController@index')->name('userProfile');
        Route::get('/edit', 'ProfileController@edit')->name('userProfileEdit');
        Route::post('/edit', 'ProfileController@edit_post')->name('userProfileEditPost');
        Route::get('/team', 'ProfileController@team')->name('userProfileTeam');
    });
});